import DefaultTheme from 'vitepress/theme'
import Demo from '../vitepress/components/vp-demo.vue'
import LayoutDemo from '../vitepress/components/vp-layout-demo.vue'

import './main.css'
import './custom.css'
import PrionectUI from '@prionect/ui'
import { createRouter, createMemoryHistory, useRouter } from 'vue-router'
import { useMockupAuth } from '@/auth/MockupAuth'
import { createAuthRoutes } from '@'
import '@vitepress-demo-preview/component/dist/style.css'

useRouter()
export default {
  ...DefaultTheme,
  enhanceApp(ctx) {
    DefaultTheme.enhanceApp(ctx)
    const router = createRouter({
      routes: createAuthRoutes(),
      history: createMemoryHistory()
    })
    ctx.app.use(router)

    ctx.app.component('Demo', Demo)
    ctx.app.component('LayoutDemo', LayoutDemo)
    ctx.app.use(PrionectUI, {
      auth: { provider: useMockupAuth() },
      i18n: { locale: 'en', messages: { de: { ui: { test: 'Dies ist ein Test aus der Vitepress-Konfiguration.' } } } }
    })

    ctx.app.component('demo-preview', Demo)
  }
}
