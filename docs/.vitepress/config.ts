import * as path from 'path'
import { type DefaultTheme, defineConfig } from 'vitepress'
// @ts-ignore
import { containerPreview, componentPreview } from '@vitepress-demo-preview/plugin'

const gitlabRepo = 'https://gitlab.com/prionect/prionect-ui'

const componentsSideBar = [
  {
    text: 'Authentication',
    collapsible: true,
    collapsed: false,
    items: [
      { text: 'Overview', link: '/components/auth/' },
      { text: 'Auth Layout', link: '/components/auth/AuthLayout' },
      { text: 'Login Form', link: '/components/auth/LoginForm' },
      { text: 'Signup Form', link: '/components/auth/SignupForm' }
    ]
  },
  {
    text: 'Base',
    collapsible: true,
    collapsed: false,
    items: [
      { text: 'Dialog', link: '/components/base/Dialog' },
      { text: 'Tabs', link: '/components/base/Tabs' },
      { text: 'Wizard', link: '/components/base/Wizard' }
    ]
  },
  {
    text: 'Data Display',
    collapsible: true,
    collapsed: false,
    items: [{ text: 'List', link: '/components/data/List' }]
  },
  {
    text: 'Form',
    collapsible: true,
    collapsed: false,
    items: [
      { text: 'Form', link: '/components/form/Form' },
      { text: 'MultiForm', link: '/components/form/MultiForm' },
      { text: 'Field', link: '/components/form/Field' },
      { text: 'Input: Autocomplete', link: '/components/input/AutocompleteInput' },
      { text: 'Input: File', link: '/components/input/FileInput' },
      { text: 'Input: Number', link: '/components/input/NumberInput' },
      { text: 'Input: Select', link: '/components/input/SelectInput' },
      { text: 'Input: Switch', link: '/components/input/SwitchInput' },
      { text: 'Input: Text', link: '/components/input/TextInput' },
      { text: 'Form Section', link: '/components/form/FormSection' }
    ]
  }
]

const guideSidebar = [
  {
    text: 'Prionect UI',
    items: [
      { text: 'Overview', link: '/guide/' },
      { text: 'Installation', link: '/guide/installation' },
      { text: 'I18n', link: '/guide/i18n' },
      { text: 'Development', link: '/guide/development' }
    ]
  }
]

export default defineConfig({
  title: 'Prionect UI',

  base: '/prionect-ui',
  cleanUrls: true,
  head: [['link', { rel: 'icon', href: '/prionect-ui/favicon.ico' }]],
  lastUpdated: true,
  markdown: {
    config(md) {
      md.use(containerPreview)
      md.use(componentPreview)
    },
    theme: 'material-theme-palenight'
  },
  themeConfig: {
    editLink: {
      pattern: gitlabRepo + '/-/edit/main/docs/:path',
      text: 'Edit this page on Gitlab'
    },
    footer: {
      message:
        'Released under the <a href="https://gitlab.com/prionect/lib/prionect-ui/blob/main/LICENSE" target="_blank">MIT License</a>.',
      copyright:
        'Copyright © Prionect GmbH, Dresden. Docs by <a href="https://vitepress.vuejs.org" target="_blank">Vitepress</a>.'
    },
    logo: '/logo.svg',
    nav: [
      { text: 'Getting started', link: '/guide/', activeMatch: '/guide' },
      { text: 'Components', link: '/components', activeMatch: '/components' }
    ],
    outline: [2, 3],
    sidebar: {
      '/components': componentsSideBar,
      '/': guideSidebar
    } as DefaultTheme.SidebarMulti,
    siteTitle: false,
    socialLinks: [
      {
        icon: {
          svg: '<svg role="img" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><title>GitLab</title><path d="m23.6004 9.5927-.0337-.0862L20.3.9814a.851.851 0 0 0-.3362-.405.8748.8748 0 0 0-.9997.0539.8748.8748 0 0 0-.29.4399l-2.2055 6.748H7.5375l-2.2057-6.748a.8573.8573 0 0 0-.29-.4412.8748.8748 0 0 0-.9997-.0537.8585.8585 0 0 0-.3362.4049L.4332 9.5015l-.0325.0862a6.0657 6.0657 0 0 0 2.0119 7.0105l.0113.0087.03.0213 4.976 3.7264 2.462 1.8633 1.4995 1.1321a1.0085 1.0085 0 0 0 1.2197 0l1.4995-1.1321 2.4619-1.8633 5.006-3.7489.0125-.01a6.0682 6.0682 0 0 0 2.0094-7.003z"/></svg>'
        },
        link: gitlabRepo
      }
    ]
  },
  vite: {
    plugins: [],
    resolve: {
      alias: {
        '@': path.resolve(__dirname, '../../src'),
        '@prionect/ui': path.resolve(__dirname, '../../src')
      },
      dedupe: ['vue', 'element-plus'] // avoid error when using dependencies that also use Vue
    },
    server: {
      host: true
    }
  }
})
