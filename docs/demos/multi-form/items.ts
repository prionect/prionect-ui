export default [
  {
    id: 1,
    name: {
      firstname: 'Kristian',
      lastname: 'Amsel'
    },
    city: 'Berlin',
    credits: 32,
    enabled: true
  },
  {
    id: 2,
    name: {
      firstname: 'Claudia',
      lastname: 'Reinhardt'
    },
    city: 'München',
    credits: 42,
    enabled: true
  },
  {
    id: 3,
    name: {
      firstname: 'Mike',
      lastname: 'Baum'
    },
    city: 'Hamburg',
    credits: 12,
    enabled: true
  },
  {
    id: 4,
    name: {
      firstname: 'Carolin',
      lastname: 'Strauss'
    },
    city: 'Hamburg',
    credits: 7,
    enabled: false
  },
  {
    id: 5,
    name: {
      firstname: 'Florian',
      lastname: 'Descher'
    },
    city: 'Berlin',
    credits: 7,
    enabled: false
  }
]
