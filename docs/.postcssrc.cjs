module.exports = {
  plugins: {
    // Enable kinda style isolation inside .vp-raw elements
    // see https://vitepress.vuejs.org/guide/markdown#raw
    'postcss-prefix-selector': {
      prefix: ':not(:where(.demo-block__content *))',
      includeFiles: [/vp-doc\.css/],
      transform(prefix, _selector) {
        const [selector, pseudo = ''] = _selector.split(/(:\S*)$/)
        return selector + prefix + pseudo
      }
    },
    tailwindcss: {},
    autoprefixer: {}
  }
}