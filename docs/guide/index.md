# Overview

<p class="mt-2 text-lg text-gray-500">Prionect UI is a Vue-based UI component library which is used in our software projects at Prionect GmbH.</p>

## Tech Stack

- [Vue 3](https://vuejs.org)
- [Element Plus](https://element-plus.org/)
- [Tailwind CSS](https://tailwindcss.com/)
- [VeeValidate](https://vee-validate.logaretm.com/v4/) (form validation)
- Typescript

## Contact

Prionect UI is developed with :heart: in the EU by

> Prionect GmbH<br/>
> Bayrische Straße 8<br/>
> 01069 Dresden<br/>
> Germany

The software is Open Source under the terms of the [MIT license](https://opensource.org/licenses/MIT).

