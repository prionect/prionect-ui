# Installation

## Requirements

The project where you want to use Prionect UI in, must fulfil the peer dependencies:

- Element Plus
- Tailwind CSS (without class prefix)

## Install NPM package

```sh
$ npm install -D @prionect/ui
```

## Add to Vue project

The library is delivered as Vue plugin, so you can add it by simply calling `App.use()`.

```typescript
// main.ts
import { createApp } from 'vue'
import App from './App.vue'

import PrionectUI from '@prionect/ui' // [!code hl]
import('@prionect/ui/dist/style.css') // [!code hl]

const app = createApp(App)
app.use(PrionectUI) // [!code hl]
app.mount('#app')
```

## Tailwind CSS configuration

The consuming project should have Tailwind CSS included. To unify styles, you should merge the Tailwind config of Prionect UI and your own.

### Override theme colors and fonts

The following example shows a `tailwind.confg.js` file in your project to change the primary color and default fonts:

```js
// tailwind.config.js
const defaultConfig = require('@prionect/ui/tailwind.config.js')
const tailwindColors = require('tailwindcss/colors')
const deepmerge = require('deepmerge')

/** @type {import('tailwindcss').Config} */
const localConfig = {
  content: ['./src/**/*.{html,js,vue}'],
  theme: {
    extend: {
      fontFamily: {
        sans: ['Nunito Sans', 'sans-serif'],
        heading: ['Nunito Sans']
      },
      colors: {
        primary: tailwindColors.amber
      }
    }
  }
}

module.exports = deepmerge(defaultConfig, localConfig)
```

You also need to embed the needed font files, usually in your global `index.html`:

~~~html
<!-- index.html -->
<html>
  <head>
    ...
    <link rel="stylesheet" href="https://rsms.me/inter/inter.css" />
  </head>
  ...
</html>
~~~

## Usage

Components are globally registered by the plugin, therefore they can be used immediately in a template:

```vue
<template>
  <div>
    <p-button>This is a button</p-button>
  </div>
</template>
```
