# Internationalization

For internationalization, Prionect UI uses `vue-i18n`.

`vue-i18n` is enabled by default, because we need it for UI labels and number formatting

## Usage in your app

You can use `vue-i18n` out of the box, for example for number formatting:
~~~vue
<template>
  {{ $n(1234.56) }}
</template>
~~~

<demo src="../demos/i18n/basic.vue" />

## Configuration

You can customize the `vue-18n` configuration by providing the `i18n` property in the plugin config object:

~~~ts{8-16}
// main ts
import PrionectUI from '@prionect/ui'
import messagesDE from '@/locales/de.json'

...

app.use(PrionectUI, {
  i18n: {
    locale: 'de',
    messages: { de: messagesDE },
    datetimeFormats: {
      myformat: {
        ...
      }
    }
  }
})

~~~

## Message strings

Message strings used by Prionect UI itself are grouped under the *"ui.*"* key namespace.

You can provide your own application messages strings as shown in the example above. They will be merged with the default message strings.

We recommend using a tool like [BabelEdit](https://www.codeandweb.com/babeledit) to manage the message files in JSON nested object format.

## Number Formats

You can use the following number formats:

<demo src="../demos/i18n/number-formats.vue" />

Example usage: 

~~~vue
<template>
   {{ $n(1789.1, 'currency' }} <!-- 1.789,10 € -->
</template>
~~~

You can set the currency unit in the i18n options (defaults to `EUR`):

~~~ts
// main.ts
app.use(PrionectUI, {
  i18n: { currency: 'USD' }
})
~~~

## Date/Time Formats

The following date/time formats are defined:

<demo src="../demos/i18n/datetime-formats.vue" />

Example Usage: 

~~~vue
<template>
   {{ $d(myDate, 'date-long' }}
</template>
~~~
