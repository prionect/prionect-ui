# Development

This page describes how to develop the `@prionect/ui` package.

## Development using the Docs

The simplest way to develop or customize components is by using this Vitepress documentation as "playground". See the existing pages under `docs/` for examples. 

Start the Vitepress development server with hot reloading:

~~~shell
$ npm run docs:dev
~~~


## Development inside another Project

Another approach is to develop the Prionect UI components inside the project where you actually need them.

To do so, first clone the repository and make it locally available using `npm link`:

~~~sh
# clone repository
$ git clone https://gitlab.com/prionect/prionect-ui.git
$ cd prionect-ui
$ npm install

# inside @prionect/ui 
$ sudo npm link
~~~

In the consuming project, you can now link the library locally:

~~~shell
# inside some-other-project/
$ npm link @prionect/ui
~~~

To rebuild the library on file changes, use `npm run watch`:

~~~shell
# inside prionect-ui
$ npm run watch
~~~

## Publish to NPM registry

~~~sh
$ npm run build
$ npm publish
~~~