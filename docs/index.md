---
layout: home

hero:
  name: Prionect UI
  text: Vue-based UI component library
  tagline: Foundation for projects at Prionect GmbH
  actions:
  - theme: brand
    text: Get Started
    link: /guide/
  - theme: alt
    text: Component Reference
    link: /components
  - theme: none
    text: View on Gitlab
    link: https://gitlab.com/prionect/prionect-ui

--- 


