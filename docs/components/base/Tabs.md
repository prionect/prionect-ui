# Tabs

`PTabs` component can be used to display data in different tabs.

## Basic Usage

<preview path="../../demos/tabs/basic.vue" />

## Badges

<preview path="../../demos/tabs/badge.vue" />

## Error state and @change 

<preview path="../../demos/tabs/error.vue" />

## API

### Props

| Name   | Description            | Type                                                                            | Default    |
|--------|------------------------|---------------------------------------------------------------------------------|------------|
| `tabs` | Tab pane configuration | Array<{id:string, label:string, badge?: string &#124; number, error?: boolean}> | (required) |


### Events

| Name      | Description        | Arguments       |
|-----------|--------------------|-----------------|
| `@change` | Active tab changed | `tabId: string` |

### Slots

| Name        | Description                               | Slot props          |
|-------------|-------------------------------------------|---------------------|
| `#tab:{id}` | Tab pane content for the tab with id `id` | `{active: boolean}` |
