# Dialog

`PDialog` is used for modal message windows.

The Dialog template is defined inside the view where you need it. You can show it by setting the `show` prop to `true` - the easiest way is to use the `v-model:show` directive:

~~~vue{8-10}
<script setup lang="ts">
import { ref } from 'vue'
const isShown = ref(false)
</script>

<template>
  <p-btn @click="isShown = true">show dialog</p-btn>
  <p-dialog v-model:show="isShown">
    ...
  </p-dialog>
</template>
~~~
## Basic Usage

<preview path="../../demos/dialog/basic.vue" />

## Danger 

You may change the button labels as needed. Use the `danger` prop to get a red button.

<preview path="../../demos/dialog/warning.vue" />

## Custom buttons

Override the `#footer` slot to add your own buttons. The methods `close()` and `confirm()` as provided as slot props.

<preview path="../../demos/dialog/custom-footer.vue" />


## API
### Props

| Name           | Description                  | Type      | Default    |
|----------------|------------------------------|-----------|------------|
| `cancelLabel`  | Cancel button text           | `string`  | `"Cancel"` |
| `confirmLabel` | Confirm button text          | `string`  | `"OK"`     |
| `danger`       | Renders a red confirm button | `boolean` | `false`    |
| `show`         | Whether to show the dialog   | `boolean` | `false`    |
| `title`        | Dialog title                 | `string`   |            |

### Events

| Name           | Description                                                 | Arguments |
|----------------|-------------------------------------------------------------|-----------|
| `@close`       | Dialog was closed (either by cancelling or confirmation)    |           |
| `@confirm`     | Dialog was confirmed (and closed)                           |           |
| `@update:show` | Fired when the dialog is closed (to support `v-model:show`) |           |


### Slots

| Name     | Description                            | Slot props                             |
|----------|----------------------------------------|----------------------------------------|
| #default | Dialog content                         |                                        |      
| #footer  | Dialog footer, used for custom buttons | `{confirm: Function, close: Function}` |      
