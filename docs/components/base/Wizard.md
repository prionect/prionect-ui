# Wizard

`PWizard` is a modal window for multi-step forms ("wizard pattern"),

There are to corresponding parts: The step configuration as defined by the `steps` prop and the respective slots in the template:

~~~vue
<script lang="ts" setup>
import { ref } from 'vue'
const isShown = ref(false)

const steps = [
    { id: 'first', title: 'First step' },
    { id: 'second', title: 'Second step' },
    { id: 'third', title: 'Third step', nextButtonLabel: 'Close' }
]
</script>

<template>
  <p-btn @click="isShown = true">show wizard</p-btn>

  <p-wizard :show="isShown" :steps="steps" @close="isShown = false" >
    <template #step:first>
      First Step
    </template>
    <template #step:second>
      Second step
    </template>
    <template #step:third>
      Third and last step
    </template>
  </p-wizard>
</template>
~~~


## Basic Usage

<preview path="../../demos/wizard/basic.vue" />


## Step Configuration

The wizard steps are defined by the `steps` prop, which is of type `WizardStepConfig[]`.

A single wizard step config item may have the following properties:

| Name                 | Type      | Description                                                                                       |
|----------------------|-----------|---------------------------------------------------------------------------------------------------|
| `id`                 | `string`  | internal ID of the step; used for named slots (step "one" becomes the slot "step:one") (required) |
| `nextButtonDisabled` | `boolean` | whether the "next" button in this step should be disabled                                         |
| `nextButtonLabel`    | `boolean` | text of the "next" button                                                                         |                      
| `title`              | `string`  | Step window title (required)                                                                      |
| `prevButtonDisabled` | `boolean` | whether the "previous" button in this step should be disabled                                     |
| `prevButtonLabel`    | `boolean` | text of the "previous" button                                                                     |                       
Example:



## Integrated `PForm`

All wizard steps are wrapped by a PForm, therefore field validation works out of the box. You can access the underlying Vee Validate `FormContext` by the form context slot prop.

All (visible) fields are automatically validated before proceeding to the next step.

## API
### Props

| Name            | Description                                      | Type               | Default    |
|-----------------|--------------------------------------------------|--------------------|------------|
| `defaultValues` | Default field values for the integrated `PForm`. | `any`              | `"Cancel"` |
| `show`          | Whether to display the wizard                    | `boolean`          | `false`    |
| `steps`         | Wizard steps configuration (see above)           | `WizardStepConfig` | `[]`       |


All other props are forwarded to the underlying [`PDialog`](./Dialog.md) component.

### Events

| Name           | Description              | Arguments |
|----------------|--------------------------|-----------|
| `@close`       | Wizard dialog was closed |           |



### Slots

| Name               | Description              | Slot props                       |
|--------------------|--------------------------|----------------------------------|
| `#default`         | Dialog content           |                                  |      
| `#step:<stepName>` | Slot for particular step | `formContext` (from VeeValidate) |      
