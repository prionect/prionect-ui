

# List

`PList` displays data records as a stacked list. 

## Simple List

By default, the item properties `title` and `subtitle` are rendered.

<preview path="../../demos/list/basic.vue" />
 
## Fixed height and selection

The list will scroll if it is taller than the container's height. When using `v-model`, key navigation of the selected item is supported:

<preview path="../../demos/list/fixed-height.vue" />

## Custom item rendering


<preview path="../../demos/list/custom.vue" />

## Loading State

<preview path="../../demos/list/loading.vue" />

## Empty State

<preview path="../../demos/list/empty.vue" />

You can customize the empty state by changing `noDataText` prop or by overriding the `noData` slot.

## API

### Props

| Name         | Description                            | Type         | Default |
|--------------|----------------------------------------|--------------|---------|
| `itemClass`  | CSS class(es) to apply to list items   | `string`     |         |
| **`items`**  | Items to display (required)            | `Array<any>` |         |
| `loading`    | whether to show loading indicator      | `boolean`    | `false` |
| `modelValue` | Selected item                          | `any`        | `null`  |
| `noBorder`   | Do not display border and shadow       | `boolean`    | `false` |
| `noDataText` | Text to show when there are no records | `string`     |         |

### Events

| Name                 | Description                         | Arguments                  |
|----------------------|-------------------------------------|----------------------------|
| `@select`            | Item was selected                   | `item: any, index: number` |
| `@update:modelValue` | Item was selected (used in v-model) | `item: any`                |

### Slots

| Name       | Description                           | Slot props                   |
|------------|---------------------------------------|------------------------------|
| `#item`    | Content of a single list item         | `{item: any, index: number}` |
| `#noData`  | Displayed when there are no items     |                              |
| `#loading` | Displayed when `loading` prop is true |                              |
