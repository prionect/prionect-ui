# Select Input

`PSelectInput` lets the user select an item from a dropdown list.

## Basic Usage

<preview path="../../demos/input/select/basic.vue" />

## Custom items

<preview path="../../demos/input/select/custom-items.vue" />

## Mixed values state

<preview path="../../demos/input/select/mixed.vue" />

## API

### Props

| Name         | Description                              | Type                                | Default    |
|--------------|------------------------------------------|-------------------------------------|------------|
| `filterable` | whether to filter items by keyboard      | `boolean`                           | `true`     |
| `items`      | list items                               | `{value: any, label: string, ...}`  | (required) |
| `itemHeight` | if set, determines the list items height | `string` (any CSS length or "auto") |            |


The components also forwards attributes to the underlying [Element Plus Select component](https://element-plus.org/en-US/component/select.html#select-attributes).

### Events

The component forwards the events from the underlying [Element Plus Select component](https://element-plus.org/en-US/component/select.html#select-events).


