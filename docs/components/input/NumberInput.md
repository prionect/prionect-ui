# Number Input

`PNumberInput` is an input element to handle numeric values. It displays and accepts them in localized notation.
## Basic Usage

<preview path="../../demos/input/number/default.vue" />

## Fraction digits

<preview path="../../demos/input/number/fraction-digits.vue" />

::: warning
Be aware that setting the `maxFractionDigits` prop leads to hard rounding of your number values. If `maxFractionDigits = 2` and the user enters `14.999`, the actual value will become `15`.
:::


## Example: Currency input

<preview path="../../demos/input/number/currency.vue" />

## Mixed values state

<preview path="../../demos/input/number/mixed.vue" />



## API

### Props

| Name                | Description                                                                                                              | Type              | Default  |
|---------------------|--------------------------------------------------------------------------------------------------------------------------|-------------------|----------|
| `grouping`          | Show thousands grouping separator                                                                                        | `boolean`         | `false`  |
| `max`               | Maximum allowed value                                                                                                    | `number`          |          |
| `maxFractionDigits` | Maximum number of fraction digits                                                                                        | `number`          | `20`     |
| `min`               | Minimal allowed value                                                                                                    | `number`          |          |
| `minFractionDigits` | Minimal number of fraction digits                                                                                        | `number`          | `0`      |
| `modelValue`        | The actual field value                                                                                                   | `number`          |          |
| `step`              | Step interval to increment or decrement the number. When set to "auto", the step will be determined by the actual value. | `number / 'auto'` | `"auto"` |
| `unit`              | Display unit suffix                                                                                                      | `string`          |          |


The components also forwards attributes to the underlying [Element Plus input component](https://element-plus.org/en-US/component/input.html).

### Events

| Name                | Description                      | Arguments       |
|---------------------|----------------------------------|-----------------|
| `change`            | Field value changed while typing | `value: number` |
| `update:ModelValue` | Field value updated (after blur) | `value: number` |

The components also forwards the events from the underlying [Element Plus input component](https://element-plus.org/en-US/component/input.html#events).


