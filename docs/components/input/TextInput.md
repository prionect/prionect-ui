# Text Input

`PTextInput` is a simple text field input.

## Basic Usage

<preview path="../../demos/input/text/basic.vue" />

## Mixed values state

<preview path="../../demos/input/text/mixed.vue" />


## API

### Props

The component forwards attributes to the underlying [Element Plus input component](https://element-plus.org/en-US/component/input.html#attributes).

### Events

The component forwards the events from the underlying [Element Plus input component](https://element-plus.org/en-US/component/input.html#events).


