# Switch Input

`PSwitchInput` is a checkbox replacement for boolean values

## Basic Usage

<preview path="../../demos/input/switch/basic.vue" />

## Hint

<preview path="../../demos/input/switch/hint.vue" />

## Sizes

<preview path="../../demos/input/switch/size.vue" /> 

## Disabled

<preview path="../../demos/input/switch/disabled.vue" />

## Mixed values state

<preview path="../../demos/input/switch/mixed.vue" />


## API

### Props

The component forwards attributes to the underlying [Element Plus input component](https://element-plus.org/en-US/component/input.html#attributes).

### Events

The component forwards the events from the underlying [Element Plus input component](https://element-plus.org/en-US/component/input.html#events).


