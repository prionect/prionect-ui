# File Input

`PFileInput` lets the user select a file which may be processed or uploaded.

Be aware that this input is not automatically handled by a `PForm`.{.warning .custom-block .pt-2}

## Basic Usage

<preview path="../../demos/input/file/basic.vue" />

## API

### Props

| Name     | Description                                                         | Type     | Default                                            |
|----------|---------------------------------------------------------------------|----------|----------------------------------------------------|
| `accept` | List of accepted file types. i.e. "\*.png,\*.jpg" oder "image/jpeg" | `string` | undefined (\*.\*)                                  |
| `label`  | Text inside the drop area                                           | `string` | "Click or drag file here." (`ui.PFileInput.label`) |

### Events

The component emits an `@update` event when a file was selected, having the respective [File](https://developer.mozilla.org/en-US/docs/Web/API/File) object as argument.

| Name     | Description             | Arguments    |
| -------- | ----------------------- | ------------ |
| `change` | A new file was selected | `file: File` |
