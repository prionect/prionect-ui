# Autocomplete Input

`PAutocompleteInput` is a textfield input that suggests value items.

## Basic Usage

<preview path="../../demos/input/autocomplete/basic.vue" />

## Mixed values state

<preview path="../../demos/input/autocomplete/mixed.vue" />

## API

### Props

The component forwards attributes to the underlying [Element Plus autocomplete component](https://element-plus.org/en-US/component/autocomplete.html#attributes).

### Events

The component forwards the events from the underlying [Element Plus autocomplete component](https://element-plus.org/en-US/component/autocomplete.html#events).


