# Component Doc Template

## Basic Usage

<preview path="../demos/list/basic.vue" />


## Extendended Usage

## API
### Props

| Name         | Description                            | Type         | Default |
|--------------|----------------------------------------|--------------|---------|


### Events

| Name                 | Description                         | Arguments                  |
|----------------------|-------------------------------------|----------------------------|

### Slots

| Name       | Description                           | Slot props                   |
|------------|---------------------------------------|------------------------------|
