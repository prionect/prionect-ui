# LoginForm

## Basic usage

<preview path="../../demos/auth/login-form.vue" />

## With social logins

<preview path="../../demos/auth/login-form-with-social.vue" />


## Social logins only

<preview path="../../demos/auth/login-form-no-password.vue" />
