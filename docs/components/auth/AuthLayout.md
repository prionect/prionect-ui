
# Auth Layout 

`PAuthLayout` is a layout view component for the login and signup screen of an application.

## Basic usage
<layout-demo src="/prionect-ui/demos/auth/layout.vue" title="Show Layout"/>

## With Login Form

<layout-demo src="/prionect-ui/demos/auth/layout-with-login.vue" title="Show Layout with Login Form"/>

## With Signup Form

<layout-demo src="/prionect-ui/demos/auth/layout-with-signup.vue" title="Show Layout with Signup Form"/>

## API

### Slots