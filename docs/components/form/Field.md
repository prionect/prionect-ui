# Field

`PField` is a configurable wrapper component to unify input handling.

## Basic usage

<preview path="../../demos/field/basic.vue" /> 

## Field types
<preview path="../../demos/field/field-types.vue" />

## Usage with v-model

<preview path="../../demos/field/v-model.vue" />


## Validation Rules

## API

### All Fields

#### All fields

### Select

#### Props

#### Events



### Password

## FieldConfig 

<<< ../../../src/components/form/types.ts#FieldConfig
 