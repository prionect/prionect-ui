# Form Section

`PFormSection` is a layout component that provides a form section header.

## Basic usage
<preview path="../../demos/form-section/basic.vue" />


## Using the `actions` Slot
<preview path="../../demos/form-section/actions.vue" />

## API

### Props

| Name    | Description       | Type     | Default |
|---------|-------------------|----------|---------|
| `title` | The section title | `string` |         |

### Slots

| Name       | Description             | Slot Props |
|------------|-------------------------|------------|
| `#actions` | Upper right section     |            |            
| `#title`   | custom title formatting |            |
