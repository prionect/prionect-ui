# Multi Form

`PMultiForm` allows bulk editing of multiple data records at once. It is based on the [Form](Form.md) component and adds the ability to handle multiple records on top of it. 

## Empty form with default values

<preview path="../../demos/multi-form/empty.vue" />

## Edit multiple items

<preview path="../../demos/multi-form/multiple.vue" />


## API

### Props

| Name          | Description                                         | Type                    | Default |
|---------------|-----------------------------------------------------|-------------------------|---------|
| `defaultItem` | Default data to use when the `items` array is empty | `Record<string, any>`  | `{}`    |
| `items`       | Array of data records                               | `Record<string, any>[]` | `[]`    |

The component also forwards props to the underlying [Form](Form.md).


### Events

| Name              | Description                                                                                              | Arguments                      |
|-------------------|----------------------------------------------------------------------------------------------------------|--------------------------------|
| `@submit`         | Form was submitted. The event arguments contains all edited items including changes.                     | `items: Record<string, any>[]` |
| `@submit:changes` | Form was submitted. The event argument contains only a set of fields having the same value in all items. | `item: Record<string, any>`    |

The component also forwards events from the underlying [Form](Form.md) (i.e. `@cancel`).


### Slots

| Name       | Description    | Slot props                                                                                 |
|------------|----------------|--------------------------------------------------------------------------------------------|
| `#default` | Default slot   | see [vee-validate `Form` slot props](https://vee-validate.logaretm.com/v4/api/form/#slots) |
| `#buttons` | Custom buttons | Vee Validate slot props & `{ onSubmit: Function, onCancel: Function }`                     |
