# Form

Prionect UI relies on [VeeValidate](https://vee-validate.logaretm.com/v4/) for form handling and validation. See [their documentation](https://vee-validate.logaretm.com/v4/guide/components/validation/) for more advanced usage.

`PForm` is designed to edit single data items. If you need to bulk edit multiple items at once, use [MultiForm](MultiForm.md).
## Basic Usage

Embed `PField` components inside a `PForm`:

~~~html
  <p-form>
    <p-field name="field1" label="First Field"/>
    <p-field name="field2" label="Second Field"/>
  </p-form>
~~~

A more realistic example:

<preview path="../../demos/form/basic.vue" />

The submit and reset buttons are automatically added by default (you may disable this by setting the `noButtons` prop).

## Loading Data

Usually a form is used to edit a data record. To set the form's values, use the `item` prop:

<preview path="../../demos/form/loading-data.vue" />

Note that you do not need to set `v-model` on the fields - this is handled internally by *vee-validate*.

## Submitting a form

When the form is submitted, it emits a `submit` event:

<preview path="../../demos/form/submit.vue" />


## Customizing the form buttons

Use the `buttons` slot to change the form's action buttons. Note tha


<preview path="../../demos/form/custom-buttons.vue" />

Note that you need to connect the `obSubmit` handler of PForm with the `@click` event of the submit Button:
~~~vue
<template>
  <p-form>
    ...
    <template #buttons="{ onSubmit }"> // [!code hl]
      <p-btn type="primary" native-type="submit" @click.prevent="onSubmit">Login</p-btn>
    </template>
  </p-form>
</template>
~~~

## Tabs inside forms

When a form contains `PTabs` and there are invalid fields, then the first tab containing an invalid field will be activated: 

<preview path="../../demos/form/tabs.vue" />

## API

### Props

| Name            | Description                                            | Type                  | Default    |
|-----------------|--------------------------------------------------------|-----------------------|------------|
| `cancelable`    | Show a cancel button                                   | `boolean`             | `false`    |
| `cancelLabel`   | Label of cancel button                                 | `boolean`             | `"Cancel"` |
| `item`          | Data record with form values                           | `Record<string, any>` |            |
| `noButtons`     | Do not render form buttons                             | `boolean`             | `false`    |
| `resetLabel`    | Label of reset button                                  | `boolean`             | `"reset"`  |
| `resettable`    | Show a reset button                                    | `boolean`             | `false`    |
| `saving`        | Whether form should display saving state               | `boolean`             | `false`    |
| `submitLabel`   | Label of submit button                                 | `boolean`             | `"Save"`   |
| `submitOnEnter` | Whether to submit the form when the user presses enter | `boolean`             | `false`    |

### Events

| Name      | Description               | Arguments                                             |
|-----------|---------------------------|-------------------------------------------------------|
| `@cancel` | Cancel button was clicked |                                                       |
| `@submit` | Form was submitted        | `values: Record<string, any>, ctx: SubmissionContext` |

### Slots

| Name       | Description    | Slot props                                                                                 |
|------------|----------------|--------------------------------------------------------------------------------------------|
| `#default` | Default slot   | see [vee-validate `Form` slot props](https://vee-validate.logaretm.com/v4/api/form/#slots) |
| `#buttons` | Custom buttons | Vee Validate slot props & `{ onSubmit: function }`                                         |

