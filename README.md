# Prionect UI

Vue-based UI component library which is used in software projects at Prionect GmbH.

Documentation: ***https://prionect.gitlab.io/prionect-ui/***

## Local Development (using the Vitepress docs):

~~~sh
$ npm install
$ npm run docs:dev
~~~
