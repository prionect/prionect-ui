const colors = require('./src/styles/colors')
const colorsAsCssPropertiesPlugin = require('./src/plugin/tailwind/colors-as-css-properties')
const tailwindToElementPlusPlugin = require('./src/plugin/tailwind/tailwind-to-element-plus')

const defaultFontFamily = '"Inter var", sans-serif'
/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{html,js,vue}', './docs/**/*.{md,vue}', './docs/.vitepress/**/*.{md,vue}'],
  corePlugins: {
    preflight: true
  },
  darkMode: 'class',
  theme: {
    colors,
    extend: {
      fontFamily: {
        sans: defaultFontFamily,
        heading: defaultFontFamily
      },
      fontSize: {
        xs: '12px',
        sm: '13px',
        base: '14px',
        lg: '15px',
        xl: '18px'
      }
    }
  },
  plugins: [colorsAsCssPropertiesPlugin, tailwindToElementPlusPlugin]
}
