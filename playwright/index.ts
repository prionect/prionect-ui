import { beforeMount } from '@playwright/experimental-ct-vue/hooks'
import PrionectUI from '@/plugin'

import '@/styles/tailwind'
import '@/styles/main.css'

beforeMount(async ({ app }) => {
  app.use(PrionectUI, { i18n: { locale: 'de' } })
})
