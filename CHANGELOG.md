# Prionect UI Changelog

## [1.0.2](https://gitlab.com/prionect/prionect-ui/compare/v1.0.1...v1.0.2) (2024-04-15)


### Bug Fixes

* Updated dependencies, including Vitepress ([bfe2e18](https://gitlab.com/prionect/prionect-ui/commit/bfe2e1885d8c2e0ed2d29341320dcadf9627ebb1))

## [1.0.1](https://gitlab.com/prionect/prionect-ui/compare/v1.0.0...v1.0.1) (2024-04-04)


### Bug Fixes

* Fixed CI Pipeline to include dist/ into package ([790ef2e](https://gitlab.com/prionect/prionect-ui/commit/790ef2ecb4fb2b384656521b1bb86b4a52e1255d))

# 1.0.0 (2024-04-04)


### Bug Fixes

* **PField:** Render label as actual <label> element (fixes [#9](https://gitlab.com/prionect/prionect-ui/issues/9)) ([5b5a1f1](https://gitlab.com/prionect/prionect-ui/commit/5b5a1f13e1187a5b321aa1b8ad6d376d773d9bef))


### Features

* Improved Element UI CSS integration to better support their size styles (fixes [#10](https://gitlab.com/prionect/prionect-ui/issues/10)) ([8c8a9f5](https://gitlab.com/prionect/prionect-ui/commit/8c8a9f5be54b260f6641314d243908d342aab533))
