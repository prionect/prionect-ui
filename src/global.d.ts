/**
 * All global or module-scoped Typescript declarations (usually interface type augmentations) should
 * be placed in this file.
 *
 * It is appended to the prionect-ui.d.ts file after the build
 *
 * Therefore, DO NOT use aliases ("@/...") in imports!
 *
 * !!!
 * IMPORTANT: If you change this file, you MUST also change the line number inside
 * the package.json file in the "types" script.
 *
 */

// EVERYTHING BELOW THIS LINE IS APPENDED AS IS ON THE prionect-ui.d.ts FILE
// -------------------------------------------------------------------------

declare module 'element-ui/lib/locale/lang/de' {}
declare module 'element-ui/lib/locale/lang/en' {}
