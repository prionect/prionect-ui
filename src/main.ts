import { createApp } from 'vue'
import App from './App.vue'

import '@/styles/main.css'

import PrionectUIPlugin from './plugin'

const app = createApp(App)
app.use(PrionectUIPlugin)
app.mount('#app')
