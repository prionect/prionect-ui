import type { AuthProviderInterface } from '@/auth'
import type { PrionectAuthOptions } from '@/plugin/types'

let authProvider: AuthProviderInterface | null = null
let options: PrionectAuthOptions | null = null

export function configure(config: { provider: AuthProviderInterface; options: PrionectAuthOptions }) {
  authProvider = config.provider
  options = config.options
}

export function useAuthentication(): AuthProviderInterface {
  if (!authProvider) {
    throw new Error('Could not use useAuthentication before configuring it.')
  }
  return {
    ...authProvider,
    options
  }
}
