import { computed, ref, watch } from 'vue'
import { useI18n } from 'vue-i18n'

// Constant to indicate that a form value has different values in multiple records
export const MIXED_VALUES = '___MIXED_VALUES___'

export function useMixedValues(props: any) {
  const { t, locale } = useI18n()

  const mixedPlaceHolderText = ref(t('ui.PForm.mixedPlaceholder'))
  watch(locale, () => {
    mixedPlaceHolderText.value = t('ui.PForm.mixedPlaceholder')
  })

  const hasMixedValue = computed(() => props.modelValue === MIXED_VALUES)
  const mixedPlaceholder = computed(() => (hasMixedValue.value ? mixedPlaceHolderText.value : props.placeholder))

  return { MIXED_VALUES, hasMixedValue, mixedPlaceholder }
}
