import type { RouteRecordRaw } from 'vue-router'
import type { Component } from 'vue'
import AuthLayout from '@/layout/AuthLayout.vue'
import { useAuthentication } from '@/index'
import LoginForm from '@/components/auth/LoginForm.vue'
import SignupForm from '@/components/auth/SignupForm.vue'

export const createAuthRoutes = (layoutComponent: Component = AuthLayout): RouteRecordRaw[] => [
  {
    name: 'auth',
    path: '/auth',
    component: layoutComponent,
    props: {},
    children: [
      {
        name: 'auth-signup',
        path: '/signup',
        component: SignupForm,
        meta: { public: true }
      },
      {
        name: 'auth-signup-confirmation',
        path: '/signup-confirmation',
        component: LoginForm,
        meta: { public: true }
      },
      {
        name: 'auth-recover',
        path: '/recover',
        component: LoginForm,
        meta: { public: true }
      },
      {
        name: 'auth-recover-confirmation',
        path: '/recover-confirmation',
        component: LoginForm,
        meta: { public: true }
      },
      {
        name: 'auth-reset-password',
        path: '/reset-password',
        component: LoginForm,
        meta: { public: true }
      },
      {
        name: 'auth-login',
        path: '/login',
        component: LoginForm,
        meta: { public: true }
      },
      {
        name: 'auth-logout',
        path: '/logout',
        component: LoginForm,
        meta: { public: true },
        beforeEnter: () => {
          useAuthentication().logout()
        }
      },
      {
        name: 'auth-forgot-password',
        path: '/forgot-password',
        component: LoginForm,
        meta: { public: true }
      }
    ]
  }
]
