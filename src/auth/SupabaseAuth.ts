import { computed, ref } from 'vue'
import { type Provider, type SupabaseClient, type User } from '@supabase/supabase-js'
import type { AuthProviderComposable } from './AuthProviderComposable'

const currentUser = ref<User | null>(null)

type SupabaseAuthOptions = {
  /**
   * The Supabase JS client instance
   */
  supabase: SupabaseClient
  redirectUrl?: string
}

/**
 * Authentication for Supabase Authentication backends
 */
export const useSupabaseAuth: AuthProviderComposable<SupabaseAuthOptions> = ({ supabase, redirectUrl }) => {
  supabase.auth.onAuthStateChange(async (event, session) => {
    if (event === 'SIGNED_IN') {
      currentUser.value = session?.user || null
      console.info('Supabase Signin', currentUser.value?.email)
    }
  })

  return {
    currentUser,

    isAuthenticated: computed(() => !!currentUser.value),

    async initialize() {},

    async login(loginData) {
      if (loginData.type === 'password') {
        // Login with username and password
        const { error } = await supabase.auth.signInWithPassword({
          email: loginData.username,
          password: loginData.password
        })
        if (error) {
          console.error(error.message)
          return false
        }
      } else if (loginData.type === 'oauth') {
        // Social login
        const { error } = await supabase.auth.signInWithOAuth({
          provider: loginData.provider as Provider,
          options: redirectUrl ? { redirectTo: redirectUrl } : undefined
        })
        if (error) {
          console.error(error.message)
          return false
        }
      }

      const { data } = await supabase.auth.getUser()
      currentUser.value = data.user

      return !!currentUser.value
    },

    async logout() {
      const { error } = await supabase.auth.signOut()
      if (error) {
        throw new Error(error.message)
      }
      currentUser.value = null
    },

    async signup(signupData) {
      const { data, error } = await supabase.auth.signUp({ email: signupData.username, password: signupData.password })
      if (error) {
        throw new Error(error.message)
      }
      const confirmationRequired = !!data.user && !data.session
      if (data.user && !confirmationRequired) {
        currentUser.value = data.user
      }
      return {
        confirmationRequired
      }
    }
  }
}
