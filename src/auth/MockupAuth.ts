import { computed, ref } from 'vue'
import type { AuthProviderComposable } from './AuthProviderComposable'

const currentUser = ref<any | null>(null)

/**
 * Authentication Mockup for testing and demo purposes
 */
export const useMockupAuth: AuthProviderComposable<void> = () => ({
  currentUser,

  isAuthenticated: computed(() => !!currentUser.value),

  async login(loginData) {
    console.info('[Auth Mockup] Login with', loginData)
    currentUser.value = { id: '1234' }
    return true
  },

  async logout() {
    console.info('[Auth Mockup] Logout')
    currentUser.value = null
  },

  async signup(signupData) {
    console.info('[Auth Mockup] Signup', signupData)
    return {
      confirmationRequired: false
    }
  }
})
