import type { Ref } from 'vue'

export type LoginData = PasswordLoginData | OAuthLoginData

type PasswordLoginData = {
  type: 'password'
  username: string
  password: string
}

type OAuthLoginData = {
  type: 'oauth'
  provider: string
}

export type SignupData = {
  username: string
  password: string
}

type SignupResponse = {
  confirmationRequired?: boolean
  userExists?: boolean
}

export interface AuthProviderInterface {
  currentUser: Ref
  initialize?: () => Promise<void>
  isAuthenticated: Ref<boolean>
  login: (options: LoginData) => Promise<boolean>
  logout: () => Promise<void>
  options?: any
  signup?: (options: SignupData) => Promise<SignupResponse>
}

export type AuthProviderComposable<O = void> = (options: O) => AuthProviderInterface
