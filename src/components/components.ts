// DATA
import { default as PList } from './data/PList.vue'

// DIALOG
import { default as PDialog } from './dialog/PDialog.vue'

// FORM
import { default as PField } from './form/PField.vue'
import { default as PForm } from './form/PForm.vue'
import { default as PFormSection } from './form/PFormSection.vue'
import { default as PMultiForm } from './form/PMultiForm.vue'
import { default as PAutocompleteInput } from './form/input/PAutocompleteInput.vue'
import { default as PFileInput } from './form/input/PFileInput.vue'
import { default as PNumberInput } from './form/input/PNumberInput.vue'
import { default as PSelectInput } from './form/input/PSelectInput.vue'
import { default as PSwitchInput } from './form/input/PSwitchInput.vue'
import { default as PTextInput } from './form/input/PTextInput.vue'

import { default as PWizard } from './wizard/PWizard.vue'

// TABS
import { default as PTabs } from './tabs/PTabs.vue'
import { ElButton } from "element-plus";

export {
    PAutocompleteInput,
    PDialog,
    PField,
    PFileInput,
    PForm,
    PFormSection,
    PList,
    PMultiForm,
    PNumberInput,
    PSelectInput,
    PSwitchInput,
    PTabs,
    PTextInput,
    PWizard,
    ElButton as PBtn,
    ElButton as PButton
}
