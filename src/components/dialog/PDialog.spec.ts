import { test, expect } from '@playwright/experimental-ct-vue'
import PDialog from './PDialog.vue'
import { ref } from 'vue'
import { vModel } from '@/util-test'

const show = ref(false)
show.value = false

let closeHaveBeenCalled = false
let confirmHasBeenCalled = false

const title = 'Dialog Title'
const content = 'Dialog content'

test.beforeEach(async ({ mount }) => {
  closeHaveBeenCalled = false
  confirmHasBeenCalled = false

  await mount(PDialog, {
    props: {
      ...vModel(show, 'show'),
      title,
      onClose: () => (closeHaveBeenCalled = true),
      onConfirm: () => (confirmHasBeenCalled = true)
    },
    slots: { default: content }
  })

  show.value = true
})

test('renders correct content', async ({ page }) => {
  const dialog = page.getByRole('dialog')
  await expect(dialog).toContainText(title)
  await expect(dialog).toContainText(content)
})

test('closes when clicking the Confirm button', async ({ page }) => {
  await page.locator('[data-el=confirm-btn]').click()
  await expect(page.getByRole('dialog')).not.toBeVisible()
  expect(confirmHasBeenCalled).toBe(true)
})

test('closes when clicking the Cancel button', async ({ page }) => {
  await page.locator('[data-el=cancel-btn]').click()
  await expect(page.getByRole('dialog')).not.toBeVisible()
  expect(closeHaveBeenCalled).toBe(true)
})

test('closes when clicking the Close icon', async ({ page }) => {
  await page.locator('[data-el=close-icon]').click()
  await expect(page.getByRole('dialog')).not.toBeVisible()
  expect(closeHaveBeenCalled).toBe(true)
})

test('closes when hitting ESC key', async ({ page }) => {
  await page.locator('body').press('Escape')
  await expect(page.getByRole('dialog')).not.toBeVisible()
  expect(closeHaveBeenCalled).toBe(true)
})

test('closes when hitting Enter key', async ({ page }) => {
  await page.locator('body').press('Enter')
  await expect(page.getByRole('dialog')).not.toBeVisible()
  expect(closeHaveBeenCalled).toBe(true)
})
