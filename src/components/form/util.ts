import deepmerge from 'deepmerge'
import { MIXED_VALUES } from '@/composables/useMixedValues'

/**
 * Find invalid fields inside tabs and make sure, the first of them is visible
 */
export function findInvalidFieldsInTabs(
  invalidFields: string[],
  formElement: HTMLFormElement
): { invalidTabs: string[] } {
  const invalidTabs: Record<string, true> = {}
  let foundFieldInATab = false

  for (let fieldName of invalidFields) {
    // find the field element by its name
    let el: HTMLElement | null = formElement.querySelector(`[data-field="${fieldName}"]`)
    let tabId = undefined

    while (el) {
      if (el === formElement) {
        // Do not bubble higher than to this form
        el = null
      } else if (el.classList.contains('p-tab')) {
        // We found the tab pane
        tabId = el.dataset.tab
        if (tabId) {
          invalidTabs[tabId] = true
        }
      } else if (el.classList.contains('p-tabs') && !foundFieldInATab) {
        // Found the PTabs element itself, find the tab header item below
        const tabHeaderItem = el.querySelector(`[data-tab-label-for="${tabId}"]`) as HTMLDivElement | null
        if (tabHeaderItem) {
          foundFieldInATab = true
          console.log('click at', tabHeaderItem, tabId)
          tabHeaderItem.click()
          el = null
        }
      }
      el = el?.parentElement || null
    }
  }

  return { invalidTabs: Object.keys(invalidTabs) }
}

//
// MULTI FORM UTILS
//

/**
 * Helper function for array handling in deepmerge
 */
function arrayMerge(a: any[], b: any[]) {
  const length = Math.max(a.length, b.length)
  const result = []
  for (let i = 0; i < length; i++) {
    result[i] = a[i] !== undefined && a[i] !== null ? (b[i] !== undefined && b[i] !== null ? b[i] : a[i]) : b[i]
  }
  return result
}

/**
 * Override existing items with new field inputs
 */
export function applyFieldsToItems(fields: any, items: any[]): Record<string, any>[] {
  const sharedValues = findNonMixedValues(fields)
  return items.map((item) => deepmerge(item, sharedValues, { arrayMerge }))
}

/**
 * Filters out all properties with MIXED_VALUES
 */
export function findNonMixedValues(data: any | any[]) {
  if (Array.isArray(data)) {
    return data.map((item) => (item !== MIXED_VALUES ? item : undefined))
  } else {
    const filteredData: any = {}
    Object.keys(data).forEach((key) => {
      if (data[key] === MIXED_VALUES) {
        return
      }
      if (typeof data[key] === 'object' && data[key] !== null) {
        filteredData[key] = findNonMixedValues(data[key])
      } else {
        filteredData[key] = data[key]
      }
    })
    return filteredData
  }
}

/**
 * Merge multiple data items into one. If values are not the same in all
 * items, they will be replaced by the MIXED_VALUES constant
 *
 * @param items
 */
export function mergeItems(items: any[]): any {
  const knownKeys: Record<string, true> = {}
  const merged: Record<string, any> = {}

  for (let i = 0; i < items.length; i++) {
    const item = items[i]
    const isFirstItem = i === 0

    // collect all known keys
    Object.keys(item).forEach((key: string) => {
      knownKeys[key] = true
    })

    // check current item for all keys
    Object.keys(knownKeys).forEach((key) => {
      const itemValue = item[key]
      const hasItemValue = itemValue !== undefined
      const previousValue = merged[key]

      if (isFirstItem) {
        // initial appearance of a value
        merged[key] = itemValue
      } else if (hasItemValue && itemValue === previousValue) {
        // do nothing, values are still equal
      } else if (hasItemValue && typeof itemValue === 'object' && typeof previousValue === 'object') {
        // Merge arrays or objects recursively
        const subMerged = mergeItems([previousValue, itemValue])
        if (Array.isArray(itemValue)) {
          merged[key] = Object.values(subMerged)
        } else {
          merged[key] = subMerged
        }
      } else {
        // anything else leads to mixed values
        merged[key] = MIXED_VALUES
      }
    })
  }
  return merged
}
