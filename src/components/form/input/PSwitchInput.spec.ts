import { test, expect } from '@playwright/experimental-ct-vue'
import PSwitchInputTest from './PSwitchInputTest.vue'

test.use({ viewport: { width: 500, height: 100 } })
test('Click on label should enable switch', async ({ mount }) => {
  const component = await mount(PSwitchInputTest, {
    props: {
      label: 'My Label'
    }
  } as any)

  await expect(component.locator('.el-switch')).not.toHaveClass(/is-checked/)
  await component.locator('label').click()
  await expect(component.locator('.el-switch')).toHaveClass(/is-checked/)
})
