<script lang="ts">
import { defineComponent } from 'vue'
import { ElInput } from 'element-plus'
import { useMixedValues } from '@/composables/useMixedValues'

/**
 * NumberInput is a wrapper around an <el-input-field> to display and enter locale number formats
 */
export default defineComponent({
  name: 'PNumberInput',
  components: { ElInput },
  emits: ['blur', 'change', 'update:modelValue'],

  props: {
    grouping: {
      type: Boolean,
      default: false
    },
    max: Number,
    maxFractionDigits: { type: Number, default: 5 },
    min: Number,
    minFractionDigits: { type: Number, default: 0 },
    modelValue: {
      type: [Number, String]
    },
    placeholder: String,
    right: Boolean,
    step: Number,
    unit: String
  },

  data: () => ({
    rawValue: null as number | null,
    displayPrefix: '',
    displaySuffix: ''
  }),

  computed: {
    /**
     * The decimal separator for the current locale
     */
    decimalSeparator(): string {
      const n = 1.1
      return this.$n(n).substring(1, 2)
    },

    /**
     * Convert the raw number value into localized format
     */
    displayValue(): string | null {
      let displayValue = ''
      if (this.rawValue !== null) {
        displayValue = this.$n(this.rawValue, {
          maximumFractionDigits: 20,
          useGrouping: this.grouping
        })
      }
      return this.displayPrefix + displayValue + this.displaySuffix
    },

    displayValueRounded(): string | null {
      if (this.rawValue !== null && !this.hasMixedValue) {
        return this.$n(this.rawValue, {
          minimumFractionDigits: this.minFractionDigits,
          maximumFractionDigits: this.maxFractionDigits,
          useGrouping: this.grouping
        })
      } else {
        return null
      }
    },

    /**
     * The thousands group separator for the current locale
     */
    groupSeparator(): string {
      const n = 1234
      const separator = this.$n(n).substring(1, 2)
      return separator !== '2' ? separator : ''
    },

    inputEl(): HTMLInputElement | undefined {
      return (this.$refs.input as any)?.$el?.querySelector('input')
    }
  },

  watch: {
    modelValue(v) {
      this.rawValue = this.convertModelValue(v)
    }
  },

  setup(props) {
    const { hasMixedValue, mixedPlaceholder } = useMixedValues(props)
    return { hasMixedValue, mixedPlaceholder }
  },

  mounted() {
    this.rawValue = this.convertModelValue(this.modelValue)

    if (this.inputEl) {
      this.inputEl.addEventListener('wheel', this.onWheel)
      this.formatValue(false, false)
    }
  },

  methods: {
    /**
     * Calculates the appropriate step width depending on the given value
     *
     * @private
     * @param value {number}
     * @param increment {boolean} true if increment, false if decrement
     * @returns {number}
     */
    calculateAutomaticStep(value: number, increment: boolean) {
      const magnitude = Math.floor(Math.log(Math.abs(value)) / Math.log(10))
      let step = Math.pow(10, magnitude)
      if ((increment && step > value) || (!increment && step >= value)) {
        step = Math.pow(10, magnitude - 2)
      }
      return Math.max(1, step)
    },

    convertModelValue(v: number | string | undefined) {
      if (typeof v === 'number') {
        return v
      } else if (typeof v === 'string') {
        const parsed = parseFloat(v)
        return isNaN(parsed) ? null : parsed
      } else {
        return null
      }
    },

    formatValue(force = false, emitEvent = true) {
      if (!this.hasMixedValue || force) {
        const floatValue = parseFloat(this.parseValue(this.displayValueRounded || ''))
        this.rawValue = isNaN(floatValue) ? null : floatValue
        if (emitEvent) {
          this.$emit('update:modelValue', this.rawValue)
        }
      }
    },

    onInput(parsedValue: string) {
      const floatValue = parseFloat(parsedValue)
      this.rawValue = isNaN(floatValue) ? null : floatValue
      this.$emit('update:modelValue', this.rawValue)
    },

    onChange() {
      if (this.rawValue !== null) {
        if (this.max !== undefined && this.rawValue > this.max) {
          this.rawValue = this.max
        }
        if (this.min !== undefined && this.rawValue < this.min) {
          this.rawValue = this.min
        }
      }
      this.$emit('change', this.rawValue)
    },

    /**
     * Event handler - Mouse wheel was used on the Input element
     * @param event {WheelEvent}
     */
    onWheel(event: WheelEvent) {
      // abort if the input field is not in focus
      if (document.activeElement !== event.target) {
        return
      }

      // increment or decrement?
      const increment = event.deltaY < 0

      // to increment/decrement whe need a valid start value
      let value = this.rawValue ? this.rawValue : 0

      // step width: use prop value or calculate step automatically (by value magnitude)
      const step = this.step ? this.step : this.calculateAutomaticStep(value, increment)

      value = Math.round(value / step) * step + (increment ? 1 : -1) * step

      if (this.min !== undefined && value < this.min) {
        value = this.min
      }
      if (this.max !== undefined && value > this.max) {
        value = this.max
      }

      this.rawValue = value
      this.formatValue(true)

      event.preventDefault()
    },

    /**
     * Parses and normalizes the localized user input
     */
    parseValue(v: string) {
      this.displayPrefix = ''
      this.displaySuffix = ''

      const isNegative = v.includes('-')

      let numberString = v
        // remove group separators
        .replaceAll(this.groupSeparator, '')
        // replace locale decimal separator by "."
        .replaceAll(this.decimalSeparator, '.')
        // remove everything else
        .replace(/[^[0-9.]/g, '')
        // remove all decimal separators but the last
        .replace(/(\.)(.*\..*)$/g, '$2')

      // Retain trailing separator (display "10," even if the rawValue is "10")
      if (numberString.slice(-1) === '.') {
        this.displaySuffix = this.decimalSeparator
      }

      // Retain trailing zeroes during input
      const matches = numberString.match(/\.(\d*?)(0+)$/)
      if (matches) {
        this.displaySuffix = (matches[1] === '' ? this.decimalSeparator : '') + matches[2]
      }

      // Minus sign handling
      if (isNegative) {
        numberString = '-' + numberString
        // display minus sign even if no number has been entered yet
        if (numberString === '-') {
          this.displayPrefix = '-'
        }
      }

      return numberString
    }
  }
})
</script>

<template>
  <el-input
    ref="input"
    v-bind="$attrs"
    class="p-number-input"
    :class="{ 'p-number-input--right': !!right }"
    :modelValue="hasMixedValue ? undefined : displayValue"
    :formatter="(v: string) => v"
    :parser="parseValue"
    :placeholder="mixedPlaceholder"
    @blur="formatValue()"
    @change="onChange"
    @update:modelValue="onInput"
  >
    <template v-if="unit" #suffix>{{ unit }}</template>
  </el-input>
</template>

<style scoped lang="css">
.p-number-input--right :deep(.el-input__inner) {
  text-align: right !important;
  padding-right: 0.5em;
}
</style>
