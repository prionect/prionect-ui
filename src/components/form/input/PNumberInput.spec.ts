import { ref } from 'vue'
import { test, expect } from '@playwright/experimental-ct-vue'
import PNumberInput from './PNumberInput.vue'
import { vModel } from '@/util-test'

const value = ref()
/*function mountField(initialValue: FieldValueType = undefined, props: any = {}) {
  value.value = initialValue

  mount(PNumberInput, {
    props: Object.assign(
      {
        label: 'My Label',
        ...vModel(value)
      },
      props
    ),
    locale: 'de'
  })
}*/

test.beforeEach(async () => {
  value.value = undefined
})

test('renders with undefined value', async ({ mount }) => {
  const component = await mount(PNumberInput)
  await expect(component.locator('input')).toHaveValue('')
})

test('renders with null value', async ({ mount }) => {
  const component = await mount(PNumberInput, { props: { modelValue: null } } as any)
  await expect(component.locator('input')).toHaveValue('')
})

test('renders with 0 value', async ({ mount }) => {
  const component = await mount(PNumberInput, { props: { modelValue: 0 } })
  await expect(component.locator('input')).toHaveValue('0')
})

test('renders with localized value', async ({ mount }) => {
  const component = await mount(PNumberInput, { props: { modelValue: 1234.567 } })
  await expect(component.locator('input')).toHaveValue('1234,567')
})

test('should convert input into localized format', async ({ mount }) => {
  let fieldValue: any

  const component = await mount(PNumberInput, {
    props: {
      grouping: true,
      modelValue: undefined
    },
    on: {
      ['update:modelValue']: (v: any) => (fieldValue = v)
    }
  })
  await component.locator('input').fill('12345,6789')
  await component.locator('input').blur()
  await expect(component.locator('input')).toHaveValue('12.345,6789')
  expect(fieldValue).toEqual(12345.6789)
})

test('should round inputs', async ({ mount }) => {
  const fieldValue = ref<any>()
  const component = await mount(PNumberInput, {
    props: { maxFractionDigits: 0, ...vModel(fieldValue) }
  })

  await component.locator('input').fill('5,6789')
  await component.locator('input').blur()
  await expect(component.locator('input')).toHaveValue('6')
  expect(fieldValue.value).toEqual(6)
})

test('should display fixed fraction digits', async ({ mount }) => {
  const fieldValue = ref<any>()
  const component = await mount(PNumberInput, {
    props: { minFractionDigits: 2, maxFractionDigits: 2, ...vModel(fieldValue) }
  })

  await component.locator('input').fill('0')
  await component.locator('input').blur()
  await expect(component.locator('input')).toHaveValue('0,00')
  expect(fieldValue.value).toEqual(0)

  await component.locator('input').fill('0,9887')
  await component.locator('input').blur()
  await expect(component.locator('input')).toHaveValue('0,99')
  expect(fieldValue.value).toEqual(0.99)
})

test('should handle invalid characters', async ({ mount }) => {
  const fieldValue = ref<any>()
  const component = await mount(PNumberInput, {
    props: { ...vModel(fieldValue) }
  })

  await component.locator('input').fill('a-0b1,2cc4')
  await component.locator('input').blur()
  await expect(component.locator('input')).toHaveValue('-1,24')
  expect(fieldValue.value).toEqual(-1.24)
})

test('should handle min/max props', async ({ mount }) => {
  const fieldValue = ref<any>()
  const component = await mount(PNumberInput, {
    props: { ...vModel(fieldValue), min: 0, max: 10 }
  } as any)

  await component.locator('input').fill('20')
  await component.locator('input').blur()
  await expect(component.locator('input')).toHaveValue('10')
  expect(fieldValue.value).toEqual(10)

  await component.locator('input').fill('-20')
  await component.locator('input').blur()
  await expect(component.locator('input')).toHaveValue('0')
  expect(fieldValue.value).toEqual(0)
})
