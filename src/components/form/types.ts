export type ValidationRule<T = any, R = boolean | string> = (v: T) => R

// #region FieldConfig
export type FieldType = 'autocomplete' | 'email' | 'number' | 'password' | 'select' | 'switch' | 'text' | 'textarea'
export type FieldConfig = BaseFieldConfig | TextFieldConfig | NumberFieldConfig | SelectFieldConfig

export type FieldConfigs = Record<string, FieldConfig>

type BaseFieldConfig<T = any> = {
  name: string
  type?: FieldType
  label?: string
  hint?: string
  placeholder?: string
  required?: boolean | string
  rules?: ValidationRule[]
}

export type TextFieldConfig = BaseFieldConfig & {
  type?: 'text' | 'autocomplete'
}

export type NumberFieldConfig = BaseFieldConfig & {
  type: 'number'
  grouping?: number
  min?: number
  minFractionDigits?: number
  max?: number
  maxFractionDigits?: number
  step?: number
  unit?: string
}

export type SelectFieldConfig = BaseFieldConfig & {
  type: 'select'
  items: Array<SelectItem | number | string>
}

export type SelectItem = { value: any; label: string }

// #endregion FieldConfig
