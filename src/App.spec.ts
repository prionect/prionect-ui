import { test, expect } from '@playwright/experimental-ct-vue'
import App from './App.vue'

test('app mount', async ({ mount }) => {
  const component = await mount(App)
  await expect(component).toContainText('App')
})
