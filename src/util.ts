export function getEnv(name: string) {
  return import.meta.env[name] || undefined
}

let sequenceNumber = 1
export function uniqueId(): string {
  return 'p-' + sequenceNumber++
}
