import type { AuthProviderInterface } from '@/auth'
import type { I18nOptions } from 'vue-i18n'

export type AuthMethod = 'password' | 'apple' | 'google' | 'facebook' | 'microsoft'

export type PrionectAuthOptions = {
  provider: AuthProviderInterface
  methods: AuthMethod[]
  enableSignup?: boolean
}
export type PrionectPluginOptions = {
  auth?: PrionectAuthOptions
  i18n?: I18nOptions & { currency?: string }
}
