import type { App } from 'vue'
import type { I18nOptions, DefineDateTimeFormat, DefineNumberFormat } from 'vue-i18n'
import { createI18n } from 'vue-i18n'
import messagesEn from '@/locales/en-US.json'
import messagesDe from '@/locales/de-DE.json'
import type { PrionectPluginOptions } from './types'
import deepmerge from 'deepmerge'

const datetimeFormats: DefineDateTimeFormat = {
  'date-short': { year: '2-digit', month: '2-digit', day: '2-digit' },
  'date-medium': { year: 'numeric', month: '2-digit', day: '2-digit' },
  'date-long': { year: 'numeric', month: 'long', day: 'numeric' },
  'weekday-short': { year: '2-digit', month: 'numeric', day: 'numeric', weekday: 'short' },
  'weekday-medium': { year: 'numeric', month: '2-digit', day: '2-digit', weekday: 'short' },
  'weekday-long': { year: 'numeric', month: 'long', day: 'numeric', weekday: 'long' },
  'datetime-short': { year: '2-digit', month: '2-digit', day: '2-digit', hour: '2-digit', minute: '2-digit' },
  'datetime-medium': { year: 'numeric', month: '2-digit', day: '2-digit', hour: '2-digit', minute: '2-digit' },
  'datetime-long': { year: 'numeric', month: 'long', day: 'numeric', hour: '2-digit', minute: '2-digit' },
  'time-short': { hour: 'numeric', minute: '2-digit' },
  'time-medium': { hour: '2-digit', minute: '2-digit' },
  'time-long': { hour: '2-digit', minute: '2-digit', second: '2-digit' }
}

const createNumberFormats = (currency: string): DefineNumberFormat => ({
  currency: { style: 'currency', currency },
  engineering: { notation: 'engineering', style: 'decimal' },
  percent: { style: 'percent' },
  rounded: { maximumFractionDigits: 0 }
})

export function install(app: App, options: PrionectPluginOptions) {
  const currency = options.i18n?.currency || 'EUR'
  const numberFormats = createNumberFormats(currency)

  const defaultOptions: I18nOptions = {
    legacy: false,
    locale: 'de',
    fallbackLocale: 'en',
    ...options.i18n,
    messages: { de: messagesDe, en: messagesEn },
    datetimeFormats: { de: datetimeFormats, en: datetimeFormats },
    numberFormats: { de: numberFormats, en: numberFormats }
  }

  const mergedOptions = options.i18n ? deepmerge(defaultOptions, options.i18n) : defaultOptions

  const i18n = createI18n(mergedOptions)
  app.use(i18n)
}

export default { install }
