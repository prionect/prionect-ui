import type { App } from 'vue'
import { configure as configureAuth } from '@/composables/useAuthentication'

import * as components from '@/components/components'
import PAuthLayout from '@/layout/AuthLayout.vue'
import { PLoginForm, PSignupForm } from '@/components/auth'
import { ElButton } from 'element-plus'
import icons from '@/styles/icons'

import i18n from './i18n'
import type { PrionectPluginOptions } from '@/plugin/types'

const install = (app: App, options: PrionectPluginOptions = {}) => {
  // configure authentication provider
  if (options.auth) {
    const provider = options.auth!.provider
    configureAuth({ provider, options: options.auth })

    if (provider.initialize) {
      provider.initialize()
    }
  }

  app.use(i18n, options)

  // register components
  const allComponents: any = { ...components, PAuthLayout, PLoginForm, PSignupForm, PBtn: ElButton, PButton: ElButton }
  Object.keys(allComponents).forEach((cmp) => {
    app.component(cmp, allComponents[cmp])
  })

  // register icons
  for (const [key, component] of Object.entries(icons)) {
    app.component('P' + key + 'Icon', component)
  }
}

export * from './types'
export default { install }
