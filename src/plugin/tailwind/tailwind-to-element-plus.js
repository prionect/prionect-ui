const plugin = require('tailwindcss/plugin')

/**
 * Convert tailwind theme to Element UI custom CSS properties
 */
module.exports = plugin(({ addBase, theme }) => {
  const properties = {}
  const mapping = {
    50: '-light-9',
    100: '-light-8',
    200: '-light-7',
    300: '-light-5',
    400: '-light-3',
    500: '',
    600: '-dark-2'
  }

  // colors
  const colors = theme('colors')
  Object.keys(colors).forEach((colorName) => {
    const color = colors[colorName]
    if (typeof color === 'string') {
      properties[`--el-color-${colorName}`] = color
    } else {
      const shades = Object.keys(color)
      shades.forEach((shade) => {
        if (mapping[shade] !== undefined) {
          properties[`--el-color-${colorName}${mapping[shade]}`] = color[shade]
        }
      })
    }
  })

  // font sizes
  const fontSizes = theme('fontSize')
  const sizeMapping = { base: 'base', xs: 'extra-small', sm: 'small', lg: 'large' }
  Object.keys(fontSizes).forEach((size) => {
    const sizeValue = typeof fontSizes[size] === 'string' ? fontSizes[size] : fontSizes[size][0]
    if (sizeMapping[size] !== undefined) {
      properties[`--el-font-size-${sizeMapping[size]}`] = sizeValue
    }
  })

  addBase({
    body: properties
  })
})
