const plugin = require('tailwindcss/plugin')

module.exports = plugin(({ addBase, theme }) => {
  function extractColorVars(colorObj, colorGroup = '') {
    return Object.keys(colorObj).reduce((vars, colorKey) => {
      const value = colorObj[colorKey]

      let newVars = {}
      if (typeof value === 'string') {
        newVars[`--tw-color${colorGroup}-${colorKey}`] = value
        if (colorKey === 'DEFAULT' || colorKey === '500') {
          newVars[`--tw-color${colorGroup}`] = value
        }
      } else {
        newVars = extractColorVars(value, `-${colorKey}`)
      }
      return { ...vars, ...newVars }
    }, {})
  }

  function extractFontFamilies(fonts) {
    const fontVars = {}
    Object.keys(fonts).forEach((fontFamily) => {
      const fontSpec = fonts[fontFamily]
      fontVars['--tw-font-' + fontFamily] = Array.isArray(fontSpec) ? fontSpec.join(', ') : fontSpec
    })
    return fontVars
  }

  addBase({ ':root': extractColorVars(theme('colors')) })
  addBase({ ':root': extractFontFamilies(theme('fontFamily')) })
})
