const tailwindColors = require('tailwindcss/colors')

module.exports = {
  primary: tailwindColors.blue,
  warning: tailwindColors.orange,
  danger: tailwindColors.red,
  error: tailwindColors.red,
  success: tailwindColors.green,
  info: tailwindColors.sky,
  gray: tailwindColors.gray,
  black: tailwindColors.black,
  white: tailwindColors.white,
  transparent: tailwindColors.transparent
}
