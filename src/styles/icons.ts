import { AlertCircle, CheckmarkCircleOutline, Close, CloudUploadOutline } from '@vicons/ionicons5'

export default {
  Error: AlertCircle,
  Close,
  Success: CheckmarkCircleOutline,
  Upload: CloudUploadOutline
}
