import './styles/main.css'

export * from './auth'
export * from './components/components'
export * from './components/form/types'

export { useAuthentication } from '@/composables/useAuthentication'

export { MIXED_VALUES } from './composables/useMixedValues'

import PrionectUIPlugin from './plugin'
export default PrionectUIPlugin

export * from './plugin'
export * from './util'
