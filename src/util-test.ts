import { type Ref } from 'vue'

/**
 * Use a Vue reactive variable as v-model in component test mounts
 *
 * Usage:
 *
 * ~~~
 *    const value = ref('myvalue')
 *    await mount(MyComponent, {
 *       props: {
 *         label: 'My Label',
 *         ...vModel(value)
 *       }
 *     })
 * ~~~
 *
 * @param modelRef The reactive variable
 * @param modelProp The model prop name (defaults to "modelValue")
 */
export function vModel<T>(modelRef: Ref<T>, modelProp = 'modelValue') {
  return {
    [modelProp]: modelRef,
    [`onUpdate:${modelProp}`]: (emittedValue: T) => {
      modelRef.value = emittedValue
    }
  }
}
